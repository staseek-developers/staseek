# STASEEK-SDB-BACK

Backend for STASEEK

***
Build and run fat jar:
1. Install JDK17: [for example openJDK](https://openjdk.java.net/install/)
2. Install maven: [the manual depends on the operating system](https://www.baeldung.com/install-maven-on-windows-linux-mac)
3. Package project (execute in the root folder of the project): `mvn clean package`
4. Run jar (execute in the target folder): `java -jar staseek-back-1.0-SNAPSHOT.jar`

***
Stub rest curl(CMD):
`curl -i -X POST -d "hello=hi" http://localhost:9004/stub/test`
package com.nsu.sdb.staseek.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/stub")
public class StubRestController {

    @PostMapping("/test")
    public String test(@RequestParam("hello") String hello) {
        return hello + "!";
    }
}

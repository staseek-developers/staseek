package com.nsu.sdb.staseek;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StaseekApplication {

    public static void main(String[] args) {
        SpringApplication.run(StaseekApplication.class, args);
    }
}

